# README #

    Milestone 1 features:
        1) Header
        2) Global Structure Tags
        3) Text Tags
        4) Images

    Milestone 2 features:
        1) Homepage already has an image.
        2) Added a menu dropdown titled Animations (with hyperlink to Text Animations Page)
        3) Added Text Animations Page that has both a form and a table.  The form is fully functional and will add entries to the table.
        4) Text Animations Form has working validation, inline-labels, a select an input and a submit button.
        5) Text Animations Table has animating text in the right-most.
        6) Text Animations Table has hyperlinks to remove items that have been added.
        7) All common CSS is in the app.css file.  Text Animation specific CSS is in its own file and scoped under the #textAnimationsPage ID tag.
        8) All code added to the repository listed above.

    Milestone 3 features:
        1) Added a carousel to the home page.
        2) Added a guest-book to the home page.  Signing will pop-up a dialog to enter the user's name.
        3) Added homepage links to other pages.
        4) Added a page full of carousel examples and animations.
        5) Added a scroll-to-top button when page scrolls down past 500 pixels.  Can be seen on longer pages like the carousel page.
        6) The scroll-to-top button uses an image since I took out the image on the homescreen (last week's requirement).

    Milestone 4 features:
        1) Added a page called Cubic-Bezier which demonstrates different values for the animation-transition values.
        2) The controller file (js/cubic-bezier.js) shows the use of loops to populate an array of objects.
        3) The controller file (js/cubic-bezier.js) also has functions to generate random color values and random cubic-bezier parameters.
        4) General Tweaks: I added some common styling for buttons and links in css/app.css.

    Milestone 5 features:
        1) Added a storage-service wrapper for local storage (/js/storage.js).  Hooked up the guest-book on the home page to use the storage-service.
           Now the guestbook outlives page refreshes.  You can delete the local-storage key ('guestbook') in the browser to clear it.
           This change can be viewed here:  https://bitbucket.org/shalati/mis-577/commits/191848709ad058b81ee5aeb69eed0f3fe4f21fb7

        2) Added a cookie called last-visit-date that stores the user's last visited date.  This date is printed on the new homepage
           footer.  It is also printed in a smart format with a new date filter called smart-date (/js/smart-date.js).  Printing is as follows:
               - Print in a time-only format if it's from the same day
               - Print in a month-day format if it's from a different day in the same year
               - Print in a month-day-year format if it's from a different year.
            This change can be viewed here: https://bitbucket.org/shalati/mis-577/commits/12ad6292a4434c5a898afe565916d972c6f9650b
            
        3) Updated this header as instructed.

    Milestone 6 features:
        (See the following files: js/color-picker.js, html/color-picker.html, css/color-picker.css)

        1) Creating and inserting elements - I dynamically created a color picker and inserted color boxes onto the page for every generated color.
        2) Change the background-color style property in response to user input - When a user hovers over one of the boxes, it updates the
           information box below (including the larger background color swatch).
        3) Updated this header as instructed.

    Milestone 7 additional feature:
        1) I have added enter and exit page transitions.  The animations and CSS are located at the bottom of css/app.css.  The top
           of this file also now has a script tag for "angular-animate.min.js".
        2) This header has been updated accordingly.