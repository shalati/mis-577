(function() {
    'use strict';

    angular
        .module('app.home', [
            'app.storage',
        ])
        .controller('HomeController', homeController);

    function homeController($scope, PageService, StorageService) {
        var vm               = this;
        vm.enterName         = enterName;
        vm.guests            = setupGuestbook();
        $scope.signGuestBook = signGuestBook;
        
        initialize();

        function initialize() {
            angular.element('#home-carousel').slick({
                dots           : true,
                arrows         : false,
                autoplay       : true,
                autoplaySpeed  : 1500,
                adaptiveHeight : true,
            });
        }

        function enterName() {
            PageService.launchDialog("#homePage", null, "Enter your name.", true, "OK", "signGuestBook", "CANCEL", "doNothing");
        }

        function signGuestBook(name) {
            if (name && name != "null") {
                vm.guests.push({ name: name, date: new Date() });
                StorageService.put('guestbook', vm.guests);
            }
        }

        function setupGuestbook() {
            return StorageService.get('guestbook') ||
            [{
                name: 'Sam Shalati',
                date: Date.parse("September 16, 2016"),
            }];
        }
    }

})();