(function() {
    'use strict';

    angular
        .module('app', [
            'ngRoute',
            'ngCookies',
            'ngAnimate',
            'ngMessages',
            'app.storage',
            'app.smartDate',
            'app.home',
            'app.dialog',
            'app.animations.text',
            'app.animations.carousel',
            'app.animations.cubic-bezier',
            'app.animations.color-picker',
        ])
        .config(appConfig)
        .run(appRun)
        .factory('PageService', pageService)
        .controller('PageController', pageController);

    function appConfig($routeProvider) {
        $routeProvider
        .when('/home', {
            templateUrl  : 'html/home.html',
            controller   : 'HomeController',
            controllerAs : 'homeVM'
        })
        .when('/animations/text', {
            templateUrl  : 'html/text-animations.html',
            controller   : 'TextAnimationsController',
            controllerAs : 'textVM'
        })
        .when('/animations/carousel', {
            templateUrl  : 'html/carousel-animations.html',
            controller   : 'CarouselAnimationsController',
            controllerAs : 'carouselVM'
        })
        .when('/animations/cubic-bezier', {
            templateUrl  : 'html/cubic-bezier.html',
            controller   : 'CubicBezierController',
            controllerAs : 'bezierVM'
        })
        .when('/animations/color-picker', {
            templateUrl  : 'html/color-picker.html',
            controller   : 'ColorPickerController',
            controllerAs : 'pickerVM'
        })
        .otherwise({redirectTo: '/home'});
    }

    function appRun($rootScope, $timeout) {
        $rootScope.$apply(documentReady());
        $rootScope.$on('$routeChangeStart',   routeChangeStartListener  );
        $rootScope.$on('$routeChangeSuccess', routeChangeSuccessListener);

        function documentReady() {
            $(document).foundation();
        }

        function pageReady() {
            $(document).foundation('topbar', 'reflow');
        }

        function routeChangeStartListener(event, next, current) {
            angular.element('#back-to-top').hide();
        }

        function routeChangeSuccessListener(event, current, previous) {
            $rootScope.$applyAsync(pageReady());
            $timeout(backToTop, 1000);
        }

        function backToTop() {
            var button = angular.element('#back-to-top');
            var main   = angular.element('#main');

            main.scroll(fadeInOut);

            function fadeInOut() {
                if (main.scrollTop() > 500) {
                    button.fadeIn();
                } else {
                    button.fadeOut();
                }
            }
        }
    }

    function pageService($rootScope) {
        return {
            launchDialog : launchDialog,
        }

        function launchDialog(selector, title, message, getInput, positiveMessage, positiveCallback, negativeMessage, negativeCallback) {
            $rootScope.$broadcast('dialogData', {
                title            : title,
                message          : message,
                getInput         : getInput,
                positiveMessage  : positiveMessage,
                negativeMessage  : negativeMessage,
                positiveCallback : "var scope = angular.element('" + selector + "').scope(); scope." + positiveCallback,
                negativeCallback : "var scope = angular.element('" + selector + "').scope(); scope." + negativeCallback,
            });
        }
    }

    function pageController($scope, $location, $cookies, PageService) {
        var vm           = this;
        vm.lastVisit     = null;
        vm.goToPath      = goToPath;
        vm.currentPath   = currentPath;
        vm.backToTop     = backToTop;
        vm.launchDialog  = PageService.launchDialog;
        $scope.doNothing = doNothing;

        initialize();

        function initialize() {
            vm.lastVisit = $cookies.getObject('last-visit-date');
            $cookies.putObject('last-visit-date', new Date());
        }

        function goToPath(path) {
            $location.path(path);
        }

        function currentPath() {
            return $location.path();
        }

        function backToTop() {
            angular.element('#main').animate({scrollTop:0}, 500);
        }

        function doNothing() {
            // You see nothing here.
        }
    }

})();