(function() {
    'use strict';

    angular
        .module('app.storage', [])
        .factory('StorageService', storageService);

    function storageService() {
        return {
            get    : get,
            put    : put,
            remove : remove,
            clear  : clear,
        }

        function get(key) {
            var string = localStorage.getItem(key);
            if (string) {
                return JSON.parse(string);
            } else {
                return null;
            }
        }

        function put(key, value) {
            localStorage.setItem(key, JSON.stringify(value));
        }

        function remove(key) {
            localStorage.removeItem(key);
        }

        function clear(key) {
            localStorage.clear();
        }
    }

})();