(function() {
    'use strict';

    angular
        .module('app.animations.color-picker', [
            'app.storage',
        ])
        .controller('ColorPickerController', colorPickerController);

    function colorPickerController($scope) {
        var vm              = this;
        vm.picker           = [];
        vm.hoveredColor     = "#ffffff";
        vm.hoveredComponent = hoveredComponent;
        
        initialize();

        function initialize() {
            // increase value
            for (var val = 0.25; val <= 1.0; val += 0.25) {
                var colors = [];
                for (var hue = 1; hue > 0.1; hue -= 0.1) {
                    colors.push(HSVtoRGB(hue, 1.0, val));
                }
                vm.picker.push(colors);
            }

            // decrease saturation
            for (var sat = 0.75; sat >= 0.25; sat -= 0.25) {
                var colors = [];
                for (var hue = 1; hue > 0.1; hue -= 0.1) {
                    colors.push(HSVtoRGB(hue, sat, 1));
                }
                vm.picker.push(colors);
            }
        }

        function colorClicked(color) {
            vm.selectedColor = color;
        }

        function getNibble(num) {
            var str = parseInt(num).toString(16);
            return (str.length == 1) ? "0" + str : str;
        }

        // ----------------------------------------
        // Credit for this function goes to Gary Tan:
        // http://axonflux.com/handy-rgb-to-hsl-and-rgb-to-hsv-color-model-c
        // ----------------------------------------
        function HSVtoRGB(h, s, v) {
            var r, g, b, i, f, p, q, t;
            i = Math.floor(h * 6);
            f = h * 6 - i;
            p = v * (1 - s);
            q = v * (1 - f * s);
            t = v * (1 - (1 - f) * s);
            switch (i % 6) {
                case 0: r = v, g = t, b = p; break;
                case 1: r = q, g = v, b = p; break;
                case 2: r = p, g = v, b = t; break;
                case 3: r = p, g = q, b = v; break;
                case 4: r = t, g = p, b = v; break;
                case 5: r = v, g = p, b = q; break;
            }
            return "#" + getNibble(Math.round(r * 255)) + getNibble(Math.round(g * 255)) + getNibble(Math.round(b * 255))
        }

        function hoveredComponent(c) {
            switch (c) {
                case 'r':
                    return parseInt(vm.hoveredColor.substring(1, 3), 16);
                case 'g':
                    return parseInt(vm.hoveredColor.substring(3, 5), 16);
                case 'b':
                    return parseInt(vm.hoveredColor.substring(5, 7), 16);
            }
        }
    }

})();