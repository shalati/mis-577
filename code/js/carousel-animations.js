(function() {
    'use strict';

    angular
        .module('app.animations.carousel', [])
        .controller('CarouselAnimationsController', carouselAnimationsController);

    function carouselAnimationsController($scope) {
        var vm = this;

        initialize();

        function initialize() {
            angular.element('#carousel-linear').slick({
                dots    : true,
                cssEase : 'linear',
            });

            angular.element('#carousel-ease-in').slick({
                dots    : true,
                cssEase : 'ease-in',
            });

            angular.element('#carousel-ease-out').slick({
                dots    : true,
                cssEase : 'cubic-bezier(0,0,0.2,1)',
            });

            angular.element('#carousel-fading').slick({
                dots    : true,
                fade    : true,
                cssEase : 'linear',
            });

            angular.element('#carousel-multi').slick({
                dots           : true,
                slidesToShow   : 3,
                slidesToScroll : 3,
            });

            angular.element('#carousel-preview').slick({
                dots         : true,
                centerMode   : true,
                slidesToShow : 1,
            });
        }
    }

})();