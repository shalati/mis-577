(function() {
    'use strict';

    angular
        .module('app.animations.text', [])
        .controller('TextAnimationsController', textAnimationsController);

    function textAnimationsController($scope) {
        var vm                = this;
        vm.tableAnimations    = [];
        vm.animationList      = [];
        vm.newAnimation       = null;
        vm.newAnimationText   = null;
        vm.pushTableAnimation = pushTableAnimation;
        vm.addAnimation       = addAnimation;
        vm.removeAnimation    = removeAnimation;

        initialize();

        function initialize() {
            vm.animationList.push({ clazz: 'blinking-text',  description: 'Blinking Text'  });
            vm.animationList.push({ clazz: 'vibrating-text', description: 'Vibrating Text' });
            vm.animationList.push({ clazz: 'moving-shadow',  description: 'Moving Shadow'  });
            vm.animationList.push({ clazz: 'font-size',      description: 'Font Size'      });

            pushTableAnimation(vm.animationList[0], 'Text 1');
            // pushTableAnimation(vm.animationList[1], 'Text 2');
            // pushTableAnimation(vm.animationList[2], 'Text 3');
            // pushTableAnimation(vm.animationList[3], 'Text 4');
        }

        function pushTableAnimation(animation, text) {
            if (animation) {
                var anim = angular.extend({ text: text }, animation);
                vm.tableAnimations.push(anim);
            }
        }

        function addAnimation() {
            if ($scope.newAnimationForm.$valid) {
                pushTableAnimation(vm.newAnimation, vm.newAnimationText);
                clearForm();
            }
        }

        function removeAnimation(animation) {
            var i = vm.tableAnimations.indexOf(animation);
            if (i >= 0 && i < vm.tableAnimations.length) {
                vm.tableAnimations.splice(i, 1);
            }
        }

        function clearForm() {
            vm.newAnimation     = null;
            vm.newAnimationText = null;
        }
    }

})();