(function() {
    'use strict';

    angular
        .module('app.dialog', [])
        .controller('DialogController', dialogController);

    function dialogController($scope) {
        var vm            = this;
        vm.data           = null;
        vm.value          = null;
        vm.dialogCallback = dialogCallback;
        
        initialize();

        function initialize() {
            $scope.$on('dialogData', dialogListener);
            angular.element(document).foundation('reveal', {});
        }

        function dialogListener(event, data) {
            vm.data = data;
            angular.element('#web-dialog').foundation('reveal', 'open');
        }

        function closeDialog() {
            angular.element('#web-dialog').foundation('reveal', 'close');
            vm.data  = null;
            vm.value = null;
        }

        function dialogCallback(callback) {
            eval(callback + "('" + vm.value + "');");
            closeDialog();
        }
    }

})();