(function() {
    'use strict';

    angular
        .module('app.animations.cubic-bezier', [])
        .controller('CubicBezierController', bezierController);

    function bezierController($scope, $timeout) {
        var vm        = this;
        vm.boxes      = [];
        vm.resetBoxes = resetBoxes;
        
        initialize();

        function initialize() {
            setupBoxes();
        }

        function setupBoxes() {
            for (var i = 0; i < 3; i++) {
                vm.boxes.push({
                    'background' : randomColor(),
                    'timing'     : randomBezier(),
                });
            }
        }

        function resetBoxes() {
            vm.boxes = [];
            $timeout(setupBoxes);
        }

        function randomColor() {
            var num = Math.random() * 0xffffff;
            return '#' + Math.floor(num).toString(16);
        }

        function randomBezier() {
            var array = [];
            for (var i = 0; i < 4; i++) {
                array.push(randomNumber());
            }
            return 'cubic-bezier(' + array.toString() + ')';
        }

        function randomNumber() {
            var num = Math.random() * 100;
            return (Math.round(num) / 100).toString();
        }
    }

})();