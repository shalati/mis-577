(function() {
    'use strict';

    angular
        .module('app.smartDate', [])
        .filter('smartDate', smartDateFilter);

    function smartDateFilter($filter) {
        return smartDate;

        function smartDate(dateString) {
            var now  = new Date();
            var date = new Date(dateString);
            var dateFilter = $filter('date');

            if (now.getFullYear() == date.getFullYear()) {
                if (now.getMonth() == date.getMonth() && now.getDate() == date.getDate()) {
                    return dateFilter(date, 'h:mm a');
                }
                return dateFilter(date, 'MMM d');
            }
            return dateFilter(date, 'M/d/yy');
        };
    }

})();